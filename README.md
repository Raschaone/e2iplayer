# E2iPlayer for E2

If you want to join the project, read wiki page 
https://gitlab.com/maxbambi/e2iplayer/wikis/home

This is a fork of original @samsamsam E2iPlayer.
It's not a copy of PRIVATE version, that I haven't got!!
I have made some changes to italian section of plugin, that I'm interesting in.

If master project reopens, I will send merge request to original one.
Anyway, thank you @samsamsam for your huge work!

@samsamsam argues that some parts of the code I have found on the internet have been stolen from his private repo.
I'm working to change these ones with code written by myself.

If you want to try my changes, simply copy the files in IPTVPlayer folder 
in box folder '/usr/lib/enigma2/python/Plugins/Extensions/IPTVPlayer'
and reboot enigma2 (rebooting only GUI is enough).

Don't copy files, before regular installation of plugin from plugin menu or ipk.
It's recommended to make a backup copy of folder.

NEW (version 2019.05.29.01 or newer) 
After first brutal copy of files, you can perform next updates in GUI, using these settings:
*  Select update server Gitlab
*  Select my nickname: maxbambi (you can choose also original e2i (no more updated) or other forks)
*  Select all types of files in packages instead of precompiled only
*  Do update and click first gitlab item in the list.
